import React from "react";
import c from "../CreateTask.module.css";
import data from "../utils";


const ServiceInstruction = () => {
    return (
        <>
            <h2 className={c.title}>Как пользоваться сервисом “Ашар”?</h2>
            <ul>
                { data.map((el, index) => {
                    return (
                        <li className={c.item} key={index}>
                            <img src={el.img} className={c.img} alt=""/>
                            <span className={c.text}>{el.title}</span>
                        </li>
                    )
                })
                }
            </ul>
        </>

    )
}

export default ServiceInstruction;