 export const instruction = [
    {
        title: "1. Разместите заказ\n" +
            "Опишите задачу, укажите сроки и бюджет",
        img: require("../../assets/images/create-task/pic1.svg")
    },

    {
        title: "2. Получите предложения\n" +
        "Исполнители сами откликнутся на ваш заказ. Обсудите детали заказа",
        img: require("../../assets/images/create-task/pic2.svg")
    },
    {
        title: "3. Выберите исполнителя\n" +
        "Выберите подходящего вам исполнителя по рейтингу, отзывам и цене",
        img: require("../../assets/images/create-task/pic3.svg")
    },
];

export default instruction;