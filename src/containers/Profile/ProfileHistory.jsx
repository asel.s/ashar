import React, {useState} from "react";
import t from "./ProfileHistory.module.css";
import som from "../../assets/images/icons/som 2.png";
import {useDispatch, useSelector} from "react-redux";
import {Link, Redirect} from "react-router-dom";
import CancelTaskModal from "./CancelTaskModal/CancelTaskModal";

const ProfileHistory = () => {

    let task = useSelector(state => state.task.task);
    const [show, setShow] = useState(false);
    const dispatch = useDispatch();

    if(task.length === 0) {
        return <Redirect to="/createTask"/>
    }

    const {id, name, summary, estimated_pay, phone_number, date_time, category} = task;

    return (
        <div className={t.profile__history}>
            <h2 className={t.title}>Список ваших заказов</h2>
            <h5 className={t.subtitle}>Ваша история</h5>
                    <div className={t.history}>
                        <h6 className={t.task_name}> {name} </h6>
                        <div className={`${t.history__item} ${t.history__bg}`}>
                            <span className={t.subcategory}>{category}</span>
                        </div>
                        <div className={t.history__item}>
                            <span className={t.history__search}>Поиск исполнителей</span>
                            <span>Создан только что</span>
                        </div>

                        <div className={t.history__item}>
                            <span> {summary} </span>
                        </div>
                        <div className={t.history__item}>
                            <div className={t.history__price}>
                                <img src={som} alt=""/>
                                <span className={t.history__left}>{estimated_pay}</span>
                            </div>
                            <div className={t.history__date}>
                                <i className="far fa-calendar-alt"/>
                                <span className={t.history__left}>Начать c {date_time}</span>
                            </div>
                            <div className={t.history__location}>
                                <i className="fas fa-map-marker-alt"/>
                                <span className={t.history__left}>Бишкек</span>
                            </div>
                        </div>

                        <div className={t.history__manage}>
                            <Link to={`/edit/${id}`} className={t.history__btn}>Редактировать</Link>
                            <button className={t.history__btn} onClick={() => setShow(true)}>Отменить заказ</button>
                        </div>
                        {
                            show ? <CancelTaskModal
                                    setShow={setShow}
                                    dispatch={dispatch}
                                    taskId={id}/>
                            : null
                        }
                    </div>
            <div className={t.profile__notice}>
                <h2 className={t.success__title}>Ваш заказ опубликован!</h2>
                <p className={t.notice}>Через некоторое время исполнители откликнутся на ваш заказ. Вам придет
                    уведомление
                </p>
            </div>
        </div>
    )
}

export default ProfileHistory;


