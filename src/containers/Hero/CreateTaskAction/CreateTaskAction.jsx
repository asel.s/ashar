import React, {useState} from "react";
import i from "../HeroInfo.module.css";
import Button from "../../../components/Button/Button";


const CreateTaskAction = (props) => {
    const [name, setName] = useState('');

    const setTask = () => {
        props.addTaskName(name);
    }
    return (
        <div className={i.hero__task}>
            <p className={i.order__info}>создавать заказы, освобождая время <br/>
                для более важных дел
            </p>
            <div className={i.task__create}>
                <p className={i.task__hint}>Создайте заказ.Например, доставить посылку</p>
                <input
                    id="input1"
                    type="text"
                    className={i.task__input}
                    placeholder="Напишите, чем вам помочь"
                    onChange={(e) => setName(e.target.value)}
                />
                <Button
                    onClick={setTask}
                    title="Создать заказ"
                    color="#FF6B2B"
                />
            </div>
        </div>

    )
}

export default CreateTaskAction;