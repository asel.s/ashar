import React from "react";
import f from './Footer.module.css';
import logo from '../../assets/images/logo-1.png';
import fb from '../../assets/images/icons/facebook.png';
import insta from '../../assets/images/icons/instagram.png';
import vk from '../../assets/images/icons/VK.png';
import tel from '../../assets/images/icons/telegram.png';
import {Link} from "react-router-dom";


const Footer = () => {
    return (
        <div className={f.footer}>
            <div className={f.footer__logo}>
                <img className={f.footer__img} src={logo} alt=""/>
            </div>
            <div className={f.footer_nav}>
                <Link to="/notFound" className={f.footer__link}> Контакты</Link>
                <Link to="/notFound" className={f.footer__link}>Создать заказ</Link>
                <Link to="/notFound" className={f.footer__link}>Найти заказы</Link>
            </div>

            <div className={f.footer__icons}>
                <form className={f.form_share}>
                    <input className={f.form__input} type="text" placeholder="Рассылка о заказах"/>
                    <input className={f.form__btn} type="submit" value="Отправить"/>
                </form>

                <ul className={f.socail_icons}>
                    <li className={f.socail_icons__item}>
                        <Link to="/notFound">
                            <img src={insta} alt=""/>
                        </Link>
                    </li>
                    <li className={f.socail_icons__item}>
                        <Link to="/notFound">
                            <img src={fb} alt=""/>
                        </Link>
                    </li>
                    <li className={f.socail_icons__item}>
                        <Link to="/notFound">
                            <img src={tel} alt=""/>
                        </Link>
                    </li>
                    <li className={f.socail_icons__item}>
                        <Link to="/notFound">
                            <img src={vk} alt=""/>
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default Footer;