import React, {useEffect, useState} from "react";
import s from './SignUp.module.css';
import {useForm} from 'react-hook-form';
import CloseButton from "../CloseButton/CloseButton";
import t from "../ToggleFormButton/ToggleFormButton.module.css";
import {useDispatch, useSelector} from "react-redux";
import a from "../Auth.module.css";
import {closeModals, signUp, toggleLoginModal} from "../../../../redux/actions/auth-action";

const SignUp = (props) => {

    // const signUpErrors = useSelector(state => state.errors.errors);
    const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
    const authError = useSelector(state => state.auth.errors);
    const {register, handleSubmit, errors} = useForm();
    const [errorUsername, serErrorUsername] = useState('');
    const dispatch = useDispatch();

    const onSubmit = (data) => {
        dispatch(signUp(data));
    }

    const handleChange = (e) => {
        if (errorUsername) {
            serErrorUsername('');
        }
    }

    useEffect(() => {

        if(isAuthenticated) {
            dispatch(closeModals());
        }
        if (authError) {
            serErrorUsername('Пользователь с таким именем уже существует')
        }
    }, [isAuthenticated, authError]);

    return (
        <div className={`${a.forms_wrapper} ${s.form_height}`}>
            <CloseButton
                dispatch={dispatch}
            />

            <h2 className={`${s.main_title} ${s.margin}`}>Для начала необходимо зарегистрироваться</h2>

            <form className={s.signup_form} onSubmit={handleSubmit(onSubmit)}>

                <h5 className={`${s.title} ${s.signUp__title}`}>Регистрация</h5>

                <div className={s.field}>
                    <input type="text"
                           id="user"
                           name="username"
                           onChange={handleChange}
                           ref={register({
                               required: "Это поле обязательно для заполнения."
                           })}/>
                    <label htmlFor="user">Имя пользователя</label>
                    {errors.username && <p className={s.error}>{errors.username.message}</p>}
                    {/*{authError && authError.username.map(el => <p className={s.error}>{el}</p>)}*/}
                    <p className={s.error}>{errorUsername}</p>
                </div>

                <div className={`${s.field} ${s.email}`}>
                    <input type="text"
                           id="email"
                           name="email"
                           ref={register({
                               required: "Это поле обязательно для заполнения.",
                               pattern: {
                                   value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                   message: "Некорректный e-mail"
                               }
                           })}
                    />
                    <label htmlFor="">E-mail</label>
                    { errors.email && <p className={s.error}>{errors.email.message}</p> }
                    {/*{authError && authError.email.map(el => <p className={s.error}>{el}</p>)}*/}
                </div>

                <div className={`${s.field} ${s.password}`}>
                    <input
                        type="password"
                        id="password"
                        name="password"
                        ref={register({
                            required: "Это поле обязательно для заполнения.",
                            minLength: {value: 8, message: "Пароль слишком короткий."}
                        })}/>
                    <label htmlFor="password">Пароль</label>
                    { errors.password && <p className={s.error}>{errors.password.message}</p>}
                    {/*{signUpErrors.password && signUpErrors.password.map(el => <p className={s.error}>{el}</p>)}*/}
                </div>

                <div className={s.field}>
                    <button className={s.btn}>Зарегистрироваться</button>
                </div>
            </form>

            <div className={t.account}>
                <span>Есть аккаунт?</span>
                <span className={t.toggle_form}
                      onClick={() => {
                          dispatch(toggleLoginModal());
                      }}
                >
                 Войдите
            </span>
            </div>
        </div>
    )
}

export default SignUp;