import {applyMiddleware, combineReducers, createStore} from "redux";
import authReducer from "./redux/reducers/auth";
import thunkMiddleware from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";
import taskReducer from "./redux/reducers/tasks";
import {returnErrors} from "./redux/actions/error-action";
import categoriesReducer from "./redux/reducers/categories";
import errorReducer from "./redux/reducers/error";

let reducers = combineReducers({
    auth: authReducer,
    errors: returnErrors,
    task: taskReducer,
    categories: categoriesReducer,
})

const store = createStore(
    reducers,
    composeWithDevTools (applyMiddleware(thunkMiddleware)),
);
// window.store = store;

export default store;