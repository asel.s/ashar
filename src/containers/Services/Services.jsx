import React from 'react'
import styles from "./Services.module.css";
import data from "./utils";
import ServicesItem from './ServicesItem';
import Title from "../../components/Title/Title";
import Button from "../../components/Button/Button";
const Services = () => {
    return (
        <div className={styles.services}>
            <div className={styles.service_block}>
             <Title size="26px" weight="bold" title="Часто пользуются этими услугами"/>
                <div className={styles.service_wrap}>
                    {data.map((elem, index) => <ServicesItem key={index} image={elem.images} title={elem.title} />)}
                </div>
                <Button
                    title="Создайте свой заказ"
                    color="#FF6B2B"
                />
            </div>
        </div>
    )
}

export default Services
