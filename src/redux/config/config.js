//Setup config/headers and token

export const tokenConfig = getState => {

    //Get token from localstorage
    const token = getState().auth.token;
    //Headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    if(token) {
        config.headers['Authorization'] = `Token ${token}`;
    }
}