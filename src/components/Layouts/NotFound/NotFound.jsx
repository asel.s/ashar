import React from "react";
import n from './NotFound.module.css'
import man from '../../../assets/images/Group 53.jpg'

const NotFound = () => {
    return (
        <div className={n.notfound}>
            <img src={man} className={n.notfound__img} alt=""/>
            <div className={n.info}>
                <h1 className={n.title}>404</h1>
                <p className={n.subtitle}>К сожалению, запрашиваемая страница не найдена</p>
            </div>
        </div>

    )
}

export default NotFound;