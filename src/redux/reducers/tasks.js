import {
    CREATE_TASK_LOADING,
    CREATE_TASK_SUCCESS,
    DELETE_LOADING, DELETE_TASK_SUCCESS,
    SET_ALL_TASKS,
    SET_TASK
} from "../actions/action-types";

let initialState = {
    // task: [],
    task: {
        // "id": 3,
        // "name": "Услуги пешего курьера",
        // "summary": "Услуги пешего курьера",
        // "estimated_pay": 600,
        // "phone_number": "0500005060",
        // "date_time": "24.06.2020 11:06",
        // "category": 3
    },
    creatingTask: false,
    allTasks: [],
    deletingTask: false
}

const setCategoryName = (id) => {
   switch (id) {
       case 2:
           return 'Курьерские услуги';
       case 3:
           return 'Ремонт и строительство';
       default:
           return null;
   }
};

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_TASK_LOADING:
            return {
                ...state,
                creatingTask: true
            }
        case CREATE_TASK_SUCCESS:
            action.payload.category = setCategoryName(action.payload.category);
            return {
                ...state,
                creatingTask: false,
                task: action.payload
            }
        case SET_ALL_TASKS:
            return {
                ...state,
                allTasks: action.payload
            }
        case DELETE_LOADING:
            return {
                ...state,
                deletingTask: true,
            }
        case DELETE_TASK_SUCCESS:
            return {
                ...state,
                deletingTask: false,
                task: []
            }
        default:
            return state;

    }
}

export default taskReducer;