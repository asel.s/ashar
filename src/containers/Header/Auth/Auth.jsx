import React     from "react";
import Login from "./Login/Login";
import SignUp from "./SignUp/SignUp";
import a from "./Auth.module.css"


const Auth = ({signUpModal}) => {
    return (
        <div className={a.modal}>
                    { signUpModal ? <SignUp /> : <Login /> }
        </div>
    )
}

export default Auth;