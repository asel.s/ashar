import React from "react";
import i from './HeroInfo.module.css'
import woman from '../../assets/images/1-ж.png'
import CreateTaskAction from "./CreateTaskAction/CreateTaskAction";

const HeroInfo = (props) => {
    return (
        <div className={i.hero__info}>
            <div className={i.hero__text}>
                <h1 className={i.hero__title}>Объединяем, помогаем</h1>
                <p className={i.hero__subtitle}>C cервисом "Ашар"  вы можете</p>
            </div>
                <CreateTaskAction addTaskName={props.addTaskName} />
            <div className={i.hero__photo}>
                <img src={woman} className={i.hero__img} alt=""/>
            </div>
        </div>
    )
}

export default HeroInfo;