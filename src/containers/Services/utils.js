const dataServices = [
    {
        title: "Курьерские  услуги",
        images: require("../../assets/images/services/1.jpg")
    },
    {
        title: "Красота и здоровье",
        images: require("../../assets/images/services/2.jpg")
    },
    {
        title: "Ремонт техники",
        images: require("../../assets/images/services/3.jpg")
    },
    {
        title: "Юридическая помощь",
        images: require("../../assets/images/services/4.jpg")
    },
    {
        title: "Уборка",
        images: require("../../assets/images/services/5.jpg")
    },
    
    {
        title: "Компьютерная помощь",
        images: require("../../assets/images/services/6.jpg")
    },
    {
        title: "Грузоперевозки",
        images: require("../../assets/images/services/7.jpg")
    },
    {
        title: "Репетиторы и курсы",
        images: require("../../assets/images/services/8.jpg")
    },
];


export default dataServices;