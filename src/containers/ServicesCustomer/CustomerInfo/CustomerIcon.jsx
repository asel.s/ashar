import React from 'react';
import som from '../../../assets/images/icons/som 2.svg';
import star from '../../../assets/images/icons/star.svg';
import comment from '../../../assets/images/icons/comment.svg';
import styles from "../Customer.module.css";

const CustomerIcon = (props) => {
    const {anotherText} = props;
    return (
        <li>
            {anotherText ?
                <p className={styles.customer_icon}>
                    <img src={som} alt=""/>
                    <span className={styles.customer_offer}> Зарабатывайте на выполнении заказов </span> </p>:
                <p className={styles.customer_icon}> <img src={star} alt=""/> <span className={styles.customer_offer}>Рейтинг</span> </p>}

            {anotherText ?
                <p className={styles.customer_icon}>
                    <img src={comment} alt=""/>
                    <span className={styles.customer_offer}> Повышайте свой рейтинг выполняя больше заказов</span> </p> :
                <p className={styles.customer_icon}> <img src={comment} alt=""/> <span className={styles.customer_offer}>Отзывы от других заказчиков</span></p>}
            {anotherText ?
                <p className={styles.customer_icon}>
                    <img src={star} alt=""/>
                   <span className={styles.customer_offer}>Просите заказчиков оценивать вашу работу, оставив отзыв.</span>
                </p> :
                <p className={styles.customer_icon}>
                    <img src={som} alt=""/>
                    <span className={styles.customer_offer}>Цены за услуги</span>
                </p>}
        </li>
    );
};

export default CustomerIcon;