import React from "react";
import t from "../Tasks.module.css";
import ava from "../../../assets/images/ava.png";
import Button from "../../../components/Button/Button";


const Performer = () => {
    return (
        <div className={t.performer}>
            <div className={t.performer__photo}>
                <img className={t.performer__img} src={ava} alt=""/>
            </div>
            <div className={t.performer__info}>
                        <span className={t.performer__text}>Чтобы выполнять заказы вы
                            должны быть исполнителем</span>
            </div>
            <Button
                title="Стать исполнителем"
                color="#FDD72C"
            />
        </div>
    )
}

export default Performer;