import React from "react";
import t from './Tasks.module.css';
import TasksItem from "./TasksItem/TasksItem";
import Performer from "./TasksAction/Performer";
import Questions from "./TasksAction/Questions";
import Button from "../../components/Button/Button";

const Tasks = () => {
    return (
        <div className={t.tasks}>
              <h1 className={t.tasks__title}>Заказы на “Ашар”</h1>
            <div className={t.tasks__block}>
                <ul className={t.tasks__list}>
                    <TasksItem/>
                </ul>
                <Button
                    title="Показать все заказы"
                    color="#FFFFFF"
                />
            </div>
            <div className={t.tasks__action}>
                <Performer/>
                <Questions/>
            </div>
        </div>
    )
}

export default Tasks;