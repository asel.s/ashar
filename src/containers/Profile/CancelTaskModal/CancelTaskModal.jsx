import React from 'react';
import c from './CancelTaskModal.module.css';
import {deleteTask} from "../../../redux/actions/tasks-action";

const CancelTaskModal = ({taskId, setShow, dispatch}) => {

    return (
        <div className={c.cancelTask}>
            <h2 className={c.title}>Отменить заказ</h2>
            <p className={c.subtitle}>Вы действительно хотите отменить?</p>
            <div className={c.btn_block}>
                <button className={`${c.btn} ${c.bg_white}`} onClick={() => {
                    dispatch(deleteTask(taskId));
                    setShow(false);
                }}>Да</button>
                <button className={`${c.btn} ${c.bg_blue}`} onClick={() => setShow(false)}>Нет</button>
            </div>
        </div>
    );
};

export default CancelTaskModal;
