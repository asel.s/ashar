import React from "react";
import d from './Dropdown.module.css'
import GuestLinks from "./GuestLinks/GuestLinks";
import {useSelector} from "react-redux";
import AuthLinks from "./AuthLinks/AuthLinks";


const Dropdown = (props) => {

    const isAuth = useSelector(state => state.auth.isAuthenticated);

    return (
        <ul className={d.dropdown}>
            { isAuth ? <GuestLinks /> : <AuthLinks /> }
        </ul>
    )
}

export default Dropdown;