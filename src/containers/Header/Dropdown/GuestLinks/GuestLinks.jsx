import React from "react";
import d from "../Dropdown.module.css";
import {useDispatch} from "react-redux";
import {logout, toggleDropdownMenu} from "../../../../redux/actions/auth-action";
import { NavLink} from "react-router-dom";


const GuestLinks = () => {

    const dispatch = useDispatch();

    const onLogout = () => {
        dispatch(logout());
        dispatch(toggleDropdownMenu(false));
    }
    return (
        <>
            <li className={d.item}>
                <NavLink
                      to="/history"
                      className={d.link}
                      activeStyle={{fontWeight: "bold" }}
                      onClick={() => dispatch(toggleDropdownMenu(false))}>
                      История
                </NavLink>
            </li>
            <li className={d.item} >
                <NavLink
                      to="/profile"
                      className={d.link}
                      activeStyle={{fontWeight: "bold" }}
                      onClick={() => dispatch(toggleDropdownMenu(false))}>
                      Профиль
                </NavLink>
            </li>
            <li className={d.item} onClick={onLogout}>
                <span className={d.link}>Выйти</span>
            </li>
        </>
    )
}

export default GuestLinks;