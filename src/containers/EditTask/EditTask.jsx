import React from 'react';
import e from './EditTask.module.css';
import c from "../CreateTask/CreateTask.module.css";
import DatePicker from "react-datepicker";
import {Controller, useForm} from "react-hook-form";
import {useDispatch, useSelector} from "react-redux";
import {editTask} from "../../redux/actions/tasks-action";


const EditTask = (props) => {

    const task = useSelector(state => state.task.task);
    const {id, name, category, summary, estimated_pay, date_time, phone_number} = task;
    const categories = useSelector(state => state.categories.categories);
    const dispatch = useDispatch();

    let new_date, new_time;

    if (date_time) {
        new_time = date_time.slice(11, 16);
        new_date = date_time.slice(0, 16).replace(/-/g, '/').replace(/(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)?/g, "$9$10$7$8$6$4$5$3$1$2");
    }

    let date_times = new Date(new_date);

    const defaultValues = {
        category: category,
        name: name,
        summary: summary,
        phone_number: phone_number,
        date: date_times,
        time: date_times,
        estimated_pay: estimated_pay,
    };

    const {register, handleSubmit, errors, control} = useForm({defaultValues});

    let dateStr;

    const onSubmit = (data) => {
        const {date, time} = data;

        // JOIN DATE AND TIME
        let timeStr = time.toString().slice(15, 24);
        formatDate(date);
        let dateTime = dateStr.concat(timeStr);
        delete data.date;
        delete data.time;

        data = {...data, date_time: dateTime, id};
        dispatch(editTask(data));
    }
    const  formatDate = (date) => {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        dateStr = [year, month, day].join('-');
        return dateStr;
    }
    return (
        <div className={e.edit_task}>
            <form className={`${c.form} ${e.form_location}`} onSubmit={handleSubmit(onSubmit)}>
                <h2 className={c.title}>Cоздайте заказ</h2>
                <div className={c.form__group}>
                    <label> Выбраная категория: <br/> </label>
                    <select
                        name="category"
                        className={c.category}
                        // value={category}
                        ref={register({required: "Required"})}
                    >
                        {categories.map((el, index) => <option key={index} value={el.id}>{el.name}</option>)}
                    </select>
                </div>

                <div className={c.form__group}>
                    <label> Заголовок </label>
                    <input
                        type="text"
                        name="name"
                        className={c.task_name}
                        // onChange={handleChangeName}
                        ref={register({required: "Required"})}
                    />
                </div>
                <div className={c.form__group}>
                    <label> Опишите задание, детали помогут выполнить заказ лучше </label>
                    <textarea
                        name="summary"
                        className={c.summary}
                        ref={register({required: "Required"})}
                    />
                </div>

                <div className={`${c.form__group}`}>
                    <label>Телефон</label><br/>
                    <input
                        type="text"
                        name="phone_number"
                        className={c.phone_number}
                        ref={register({required: "Required"})}
                    />
                </div>
                <div className={`${c.form__group} ${c.inline}`}>

                    <Controller
                        as={<DatePicker/>}
                        name="date"
                        valueName="selected"
                        onChange={([selected]) => selected}
                        minDate={new Date()}
                        dateFormat='dd/MM'
                        control={control}
                    />

                    <Controller
                        as={DatePicker}
                        name="time"
                        valueName="selected"
                        onChange={([selected]) => selected}
                        showTimeSelect
                        showTimeSelectOnly
                        timeCaption="Time"
                        dateFormat="HH:mm"
                        control={control}
                    />
                </div>

                <div className={c.form__group}>
                    <label>Укажите цену:</label>
                    <input
                        id="input_price"
                        name="estimated_pay"
                        className={c.price}
                        placeholder="500"
                        ref={register({required: "Required"})}
                    />
                </div>
                <button type="submit" className={c.button}>Сделать заказ</button>
            </form>
            {/*<div className={e.aboutService}>*/}
            {/*    About Service*/}
            {/*</div>*/}
        </div>
    );
};


export default EditTask;
