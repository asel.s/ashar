import React from 'react';
import styles from "./Customer.module.css";
import customer from "../../assets/images/customer.png";
import ServicesSection from "./ServicesSection/ServicesSection";
import user from "../../assets/images/user.png";
const ServicesCustomer = () => {
    return (
        <div className={styles.customer}>
            <div className={styles.customer_section}>
                <ServicesSection
                    customer={customer}
                    title="Как пользоваться сервисом “Ашар"
                    secondTitle="Заказчикам"
                />
            </div>
            <div className={styles.customer_section}>
                <ServicesSection
                    user={user}
                    title="Как пользоваться сервисом Ашар"
                    secondTitle="Исполнителям"
                    reverse={true}
                />
            </div>
        </div>
    );
};

export default ServicesCustomer;