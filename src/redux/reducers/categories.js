import {SET_CATEGORIES, SET_SUBCATEGORIES, ITEMS_LOADING} from "../actions/action-types";

let initialState = {
    categories: [],
    subcategories: [],
    loading: false,
}

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_CATEGORIES:
            return {
                ...state,
                categories: action.payload,
                loading: false
            };
        case SET_SUBCATEGORIES:
            return {
                ...state,
                subcategories: action.payload,
                loading: false
            };
        case ITEMS_LOADING:
            return {
                ...state,
                loading: true
            }
        default:
            return state;
    }
}

export default categoriesReducer;