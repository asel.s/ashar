import React from "react";
import d from "../Dropdown.module.css";
import {toggleDropdownMenu, toggleLoginModal, toggleSignUpModal} from "../../../../redux/actions/auth-action";
import {useDispatch} from "react-redux";


const AuthLinks = () => {

    const dispatch = useDispatch();

    const onClickLogin = () => {
        dispatch(toggleLoginModal());
        dispatch(toggleDropdownMenu(false));
        document.body.style.overflow = 'hidden';
    }

    const onClickSignup = () => {
        dispatch(toggleSignUpModal());
        dispatch(toggleDropdownMenu(false));
        document.body.style.overflow = 'hidden';
    }

    return (
        <>
            <li className={d.item} onClick={onClickLogin}>
                <span className={d.link}>Войти</span>
            </li>
            <li className={d.item} onClick={onClickSignup}>
                <span className={d.link}>Регистрация</span>
            </li>
        </>
    )
}

export default AuthLinks;