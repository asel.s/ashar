import React from "react";
import t from "../Tasks.module.css";
import {Link} from "react-router-dom";


const TasksItem = () => {
    return (
        <>
            <li className={t.tasks__item}>
                <Link to={'/details'} className={t.tasks__link}>Покрытие гель-лак 700с</Link>
            </li>
            <li className={t.tasks__item}>
                <Link to={'/detail'} className={t.tasks__link}> Доставить документы по заданному адресу 200с</Link>
            </li>
            <li className={t.tasks__item}>
                <Link to={'/details'} className={t.tasks__link}>Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit,</Link>
            </li>
            <li className={t.tasks__item}>
                <Link to={'/detail'} className={t.tasks__link}>Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit,</Link>
            </li>
        </>
    )
}

export default TasksItem;