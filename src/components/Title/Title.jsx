import React from 'react'

const Title = (props) => {
    const {title, size, weight} = props;
    return (
        <div style={{fontSize: size, fontWeight: weight}}>
            {title}
        </div>
    )
}

export default Title
