import React from 'react'
import styles from "./Services.module.css"
const ServicesItem = (props) => {
    const {image, title} = props;
    return (
        <div className={styles.services_item}>
            <img src={image} alt={title}/>
            <div>{title}</div>
        </div>
    )
}

export default ServicesItem
