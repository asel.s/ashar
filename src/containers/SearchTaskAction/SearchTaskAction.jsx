import React from "react";
import s from './SearchTaskAction.module.css'
import man from '../../assets/images/1-м.png'
import Button from "../../components/Button/Button";

const SearchTaskAction = () => {
    return (
        <div className={s.search_wrap}>
            <div className={s.search}>
                <div className={s.search__man}>
                    <img className={s.search__img} src={man} alt=""/>
                </div>
                <div className={s.search__task}>
                    <p className={s.search__info}>зарабатывать на том, что вы умеете
                        став исполнителем</p>
                    <div className={s.search__block}>
                        <p className={s.search__hint}>Напишите, что вы можете сделать или просто нажмите и посмотрите, что есть</p>
                        <input
                            id="input1"
                            type="text"
                            className={s.search__input}
                            placeholder="Напишите, чем вам помочь"
                            // onChange={(e) => setName(e.target.value)}
                        />
                        <Button
                            title="Найти заказ"
                            color="#FDD72C"
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SearchTaskAction;