import * as axios from "axios";

const instance = axios.create({
    withCredentials: true,
    // baseURL: 'http://localhost:8000/api/v1'
    baseURL: 'http://ashardev.herokuapp.com/api/v1'
})

export const authAPI = {
    registration(username, email, password) {
        return instance.post(`/auth/register`, {username, email, password});
    },
    login(username, password) {
        return instance.post(`/auth/login`, {username, password});
    },
    getUser(getState) {
        const token = getState().auth.token;

        const config = {
            headers: {
                'Content-type': 'application/json'
            }
        };
        if (token) {
            config.headers['Authorization'] = `Token ${token}`;
        }
        return instance.get('/auth/user', config);
    },
    logout(getState) {
        const token = getState().auth.token;
        const config = {
            headers: {
                'Content-type': 'application/json'
            }
        };
        if (token) {
            config.headers['Authorization'] = `Token ${token}`;
        }
        return instance.post(`/auth/logout`, null, config);
    }
}

export const categoriesAPI = {
    getCategories() {
        return instance.get(`/categories/`)
    },
    getSubcategories() {
        return instance.get(`/subcategories/`)
    },
}
export const taskAPI = {

    createTask(data, getState) {
        const token = getState().auth.token;
        const config = {
            headers: {
                'Content-type': 'application/json'
            }
        };
        if (token) {
            config.headers['Authorization'] = `Token ${token}`;
        }
        return instance.post(`/tasks/`, data, config)
    },

    deleteTask(taskId, getState) {

        const token = getState().auth.token;
        const config = {
            headers: {
                'Content-type': 'application/json'
            }
        };
        if (token) {
            config.headers['Authorization'] = `Token ${token}`;
        }

        return instance.delete(`/tasks/${taskId}/`, config)
    },

    editTask(data) {
        // const token = getState().auth.token;
        // const config = {
        //     headers: {
        //         'Content-type': 'application/json'
        //     }
        // };
        // if (token) {
        //     config.headers['Authorization'] = `Token ${token}`;
        // }
        let taskId = data.id;
        delete data.id;
        debugger;
        return instance.put(`/tasks/${taskId}/`, data)
    },

    getTask() {
        return instance.get(`/tasks/`)
    }
}