import {categoriesAPI} from "../../  api/api";
import {ITEMS_LOADING, SET_CATEGORIES, SET_SUBCATEGORIES} from "./action-types";
import {returnErrors} from "./error-action";


export const setCategories = () => (dispatch) => {
    dispatch(setItemsLoading());
    categoriesAPI.getCategories()
    .then(response => {
            dispatch({
                type: SET_CATEGORIES,
                payload: response.data
            })
        }).catch(error => {
            dispatch(returnErrors(error.response.data,
                error.response.status));
        })
}

export const setSubcategories = () => (dispatch) => {
    dispatch(setItemsLoading());
    categoriesAPI.getSubcategories()
        .then(response => {
            dispatch({
                type: SET_SUBCATEGORIES,
                payload: response.data
            })
        }).catch(error => {
        dispatch(returnErrors(error.response.data,
            error.response.status));
    })
}

export const setItemsLoading = () => {
    return {
        type: ITEMS_LOADING
    }
}