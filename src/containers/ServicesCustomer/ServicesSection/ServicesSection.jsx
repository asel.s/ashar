import React from 'react';
import styles from "../Customer.module.css";
import Title from "../../../components/Title/Title";
import CustomerInfo from "../CustomerInfo/CustomerInfo";

const ServicesSection = (props) => {
    const {title, secondTitle, customer, reverse, user} = props;
    return (
        <>
            {!reverse ?
                <>
                    <div className={styles.customer__title}>
                        <Title
                            size="40px"
                            weight="normal"
                            title={title}
                        />
                        <h3>{secondTitle}</h3>
                    </div>
                    <div className={styles.customer_wrap}>
                        <div className={styles.customer_left}>
                            <CustomerInfo/>
                        </div>
                        <img src={customer} alt={customer} className={styles.customer_img}/>
                    </div>
                </> :
                <>
                    <div className={styles.customer__title}>
                        <Title
                            size="40px"
                            weight="normal"
                            title={title}
                        />
                        <h3>{secondTitle}</h3>
                    </div>
                    < div className={styles.customer_wrap}>
                        <img src={user} alt={user} style={{marginRight: "120px"}} />
                        <div className={styles.customer_left}>
                            <CustomerInfo anotherText={true} />
                        </div>
                    </div>
                </>
            }
        </>
    );
};

export default ServicesSection;