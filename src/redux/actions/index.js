const ORDERS_LOADED = 'ORDERS_LOADED';


export const ordersLoaded = (newOrder) => ({ type: ORDERS_LOADED, payload: newOrder});


