import {
    AUTH_ERROR, CLOSE_MODALS, LOGIN_ERROR, LOGIN_SUCCESS, LOGOUT_SUCCESS,
    SIGN_UP_ERROR,
    SIGN_UP_START,
    SIGN_UP_SUCCESS,
    TOGGLE_DROPDOWN_MENU, TOGGLE_LOGIN_MODAL, TOGGLE_SIGNUP_MODAL,
    USER_LOADED, USER_LOADING
} from "../actions/action-types";
import {getUserFromLocaleStorage} from "./utils";


let initialState = {
    token: getUserFromLocaleStorage('currentUser'),
    isAuthenticated: null,
    loading: false,
    user: null,
    errors: [],
    // isModalForm: false,
    isSignUpModal: false,
    isLoginModal: false,
    isDropdown: false,
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOADING:
            return {
                ...state,
                loading: true
            };
        case USER_LOADED:
            return {
                ...state,
                isAuthenticated: true,
                loading: false,
                user: action.payload
            };
        case SIGN_UP_START:
            return {
                ...state,
                loading:  true
            }
        case LOGIN_SUCCESS:
        case SIGN_UP_SUCCESS:
            localStorage.setItem('token', action.payload.token);
            return {
                ...state,
                ...action.payload,
                isAuthenticated: true,
                loading: false
            }
        case AUTH_ERROR:
        case SIGN_UP_ERROR:
        case LOGIN_ERROR:
        case LOGOUT_SUCCESS:
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                user: null,
                isAuthenticated: false,
                loading: false,
                errors: action.payload,

            }
        case TOGGLE_DROPDOWN_MENU:
            return {
                ...state,
                isDropdown: action.payload
            }
            case TOGGLE_LOGIN_MODAL:
            return {
                ...state,
                isLoginModal: true,
                isSignUpModal: false
            }
            case TOGGLE_SIGNUP_MODAL:
            return {
                ...state,
                isSignUpModal: true,
                // isLoginModal: false,
            }
            case CLOSE_MODALS:
            return {
                ...state,
                isSignUpModal: false,
                isLoginModal: false,
            }

        default:
            return state;
    }
}







export default authReducer;