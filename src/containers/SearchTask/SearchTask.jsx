import React, {useState} from "react";
import s from './SearchTask.module.css'
import CustomerIcon from "../ServicesCustomer/CustomerInfo/CustomerIcon";


const SearchTask = () => {
    const categories = ['Курьерские услуги', 'Красота и здоровье', 'Уборка', 'Ремонт техники', 'Репетиторы и курсы', 'Компьютерная помощь', 'Юридическая помощь'];
    const [item, setItem] = useState('');

    const handleChange = (e) => {
        setItem(e.target.value);
    }
    return (
        <div className={s.searchTask}>
            <div className={s.info}>
                <h2 className={s.title}>Найдите заказы </h2>
                <p className={s.subtitle}>Выберите категории заданий, которые вам будет интересно выполнять.
                    По этим категориям вы будете получать рассылку
                </p>
            <div>
                    <form>
                        {
                            categories.map((elem) => {
                                return (
                                    <div className={s.task_item}>
                                        <input
                                            type="checkbox"
                                            id="category"
                                            value={elem}
                                            onChange={handleChange}/>
                                        <label htmlFor="category">{elem}</label>
                                    </div>
                                )
                            })
                        }
                        <p className={s.hint}> Чтобы вы могли вы могли выполнять заказы, <br/>
                            необходимо ввести свой номер </p>
                        <div className={s.task_phone}>
                            <input
                                type="text"
                                id="phone"
                                name="phone"
                                placeholder="(996)550-88-88-88"
                            />
                            <label htmlFor="phone">Телефон</label>
                        </div>
                        <button className={s.btn}>Сохранить</button>
                    </form>
                </div>
            </div>

            <div className={s.rating}>
                <CustomerIcon anotherText={true}/>
            </div>
        </div>
    )
}

export default SearchTask;