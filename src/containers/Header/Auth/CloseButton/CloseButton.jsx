import React from "react";
import c from "./CloseButton.module.css";
import close from '../../../../assets/images/close.png'
import {closeModals} from "../../../../redux/actions/auth-action";


const CloseButton = (props) => {

    const handleClick = () => {
        props.dispatch(closeModals());
        document.body.style.overflow = '';
    }

    return (
        <div>
            <img src={close} className={c.x_btn} onClick={handleClick} alt=""/>
        </div>
    )
}

export default CloseButton;