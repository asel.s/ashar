import React from 'react';

const Button = (props) => {
    const {title, color, margin} = props;
    return <button style={{background: color, marginTop: margin ? "50px" : null}}>{title}</button>;
};

export default Button;