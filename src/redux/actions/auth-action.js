import {
    AUTH_ERROR, CLOSE_MODALS,
    LOGIN_ERROR,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    SIGN_UP_ERROR,
    SIGN_UP_START,
    SIGN_UP_SUCCESS,
    TOGGLE_DROPDOWN_MENU,
    TOGGLE_LOGIN_MODAL,
    TOGGLE_SIGNUP_MODAL,
    USER_LOADED, USER_LOADING
} from "./action-types";
import {authAPI} from "../../  api/api";
import {returnErrors} from "./error-action";


export const toggleLoginModal = () => ({
    type: TOGGLE_LOGIN_MODAL
});

export const toggleSignUpModal = () => ({
    type: TOGGLE_SIGNUP_MODAL
});

export const closeModals = () => ({
    type: CLOSE_MODALS
});

export const toggleDropdownMenu = (value) => ({type: TOGGLE_DROPDOWN_MENU, payload: value});

// Check token and load user

export const loadUser = () => (dispatch, getState) => {
    //User Loading
    dispatch({type: USER_LOADING});

    authAPI.getUser(getState)
        .then(response => {
            dispatch({
                type: USER_LOADED,
                payload: response.data
            })
        })
        .catch(error => {
            dispatch(returnErrors(error.response.data, error.response.status,));
            dispatch({type: AUTH_ERROR});
        })
}

export const signUp = ({username, email, password,}) => (dispatch) => {
    dispatch({type: SIGN_UP_START})
    authAPI.registration(username, email, password)
        .then(response => {
            if (response.status === 200) {
                dispatch({
                    type: SIGN_UP_SUCCESS,
                    payload: response.data
                })
            }
        }).catch(error => {
        dispatch({type: SIGN_UP_ERROR, payload: error.response.data});
    });
}

export const login = ({username, password}) => (dispatch) => {
    authAPI.login(username, password)
        .then(response => {
            if (response.status === 200) {
                dispatch({
                    type: LOGIN_SUCCESS,
                    payload: response.data
                })
            }
        }).catch(error => {
            // dispatch(returnErrors(error.response.data, error.response.status));
        dispatch({type: LOGIN_ERROR, payload: error.response.data});
    })
}
    export const logout = () => (dispatch, getState) => {
        authAPI.logout(getState)
            .then(response => {
                dispatch({
                    type: LOGOUT_SUCCESS,
                })
            }).catch(error => {
            dispatch(returnErrors(error.response.data,
                error.response.status));
        })
    }


