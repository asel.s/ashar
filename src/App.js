import React, {useEffect} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Login from "./containers/Header/Auth/Login/Login";
import SignUp from "./containers/Header/Auth/SignUp/SignUp";
import HomePage from "./containers/HomePage/HomePage";
import NotFound from "./components/Layouts/NotFound/NotFound";
import ProfileHistory from "./containers/Profile/ProfileHistory";
import CreateTask from "./containers/CreateTask/CreateTask";
import SearchTask from "./containers/SearchTask/SearchTask";
import Footer from "./containers/Footer/Footer";
import {loadUser} from "./redux/actions/auth-action";
import store from "./store";
import {useDispatch} from "react-redux";
import {setCategories} from "./redux/actions/categories-action";
import Profile from "./containers/Profile/Profile";
import {getTasks} from "./redux/actions/tasks-action";
import EditTask from "./containers/EditTask/EditTask";
import Header from "./containers/Header/Header";

function App() {

    const dispatch = useDispatch();

    useEffect(() => {
        store.dispatch(loadUser());
        dispatch(setCategories());
        dispatch(getTasks());
    }, []);

    return (
        <div className="container">
            <Header/>
            <Switch>
                <Route exact path='/' render={() => <HomePage/>}/>
                <Route path="/login" component={Login}/>
                <Route path="/signup" component={SignUp}/>
                <Route path="/tasksList" render={() => <NotFound/>}/>
                <Route path="/profileHistory" render={() =><ProfileHistory/>}/>
                <Route path="/createTask" render={() =><CreateTask/>}/>
                <Route path="/searchTask" render={() =><SearchTask/>}/>
                <Route path="/history" render={() =><ProfileHistory/>}/>
                <Route path="/profile" render={() =><Profile/>}/>
                <Route path="/notFound" render={() =><NotFound/>}/>
                <Route path="/edit" render={() =><EditTask/>}/>
            </Switch>
            <Footer/>
        </div>

    );
}

App.displayName = 'App';

export default App;
