import React from 'react';
import SearchTaskAction from "../SearchTaskAction/SearchTaskAction";
import Services from "../Services/Services";
import ServicesCustomer from "../ServicesCustomer/ServicesCustomer";
import Tasks from "../Tasks/Tasks";
import HeroInfo from "../Hero/HeroInfo";

const HomePage = () => {

    return (
        <>
            <HeroInfo/>
            <SearchTaskAction/>
            <Services/>
            <ServicesCustomer/>
            <Tasks/>
        </>
    );
};

export default HomePage;