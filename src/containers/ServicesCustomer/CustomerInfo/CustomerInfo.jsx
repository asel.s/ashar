import React from 'react';
import styles from "../Customer.module.css";
import {Link} from "react-router-dom";
import Button from "../../../components/Button/Button";
import CustomerInfoItem from "./CustomerInfoItem";
import CustomerIcon from "./CustomerIcon";

const CustomerInfo = (props) => {
    const {anotherText} = props;
    console.log(anotherText)
    return (
        <div className={styles.customer_info}>
            <ul className={styles.customer_list}>
                <li>
                    {anotherText ? <CustomerInfoItem title="1.Пройдите регистрацию"/> :
                        <CustomerInfoItem title="1.Создайте задание"/>}
                    {anotherText ? <br/> : <span>Опишите задачу, которую требуется выполнить.</span>}
                    <Link
                        to={anotherText ? "/register" : "/create"}>{anotherText ? "Регистрация" : "Создать задание"}</Link>
                </li>

                <li>{anotherText ?
                    <CustomerInfoItem title="2. Выберите категорию заказов, который вы можете выполнять"/> :
                    <CustomerInfoItem
                        title="2. Ожидайте. Через некоторое время кандидаты предложат вам свои услуги и цены"/>}
                </li>
                <li>{anotherText ? <CustomerInfoItem
                        title="3. Получите оплату сразу после завершения заказа"/> :
                    <CustomerInfoItem
                        title="3. Выбирайте кандидатов для выполнения вашего заказа, для этого можете посмотреть на их"/>}</li>
                <CustomerIcon anotherText={anotherText}/>
            </ul>
            {anotherText ?
                <Button
                    margin
                    title="Регистрация"
                    color="#FDD72C"
                /> :
                <Button
                    title="Разместите задание"
                    color="#FF6B2B"
                />
            }
        </div>
    );
};

export default CustomerInfo;