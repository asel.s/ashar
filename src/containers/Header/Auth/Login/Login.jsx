import React, {useEffect, useState} from "react";
import l from "./login.module.css"
import {useForm} from 'react-hook-form';
import CloseButton from "../CloseButton/CloseButton";
import t from "../ToggleFormButton/ToggleFormButton.module.css";
import {useDispatch, useSelector} from "react-redux";
import a from '../Auth.module.css'
import {
    closeModals,
    login,
    toggleSignUpModal
} from "../../../../redux/actions/auth-action";


const Login = (props) => {

    const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
    const authError = useSelector(state => state.auth.errors);
    const dispatch = useDispatch();
    const [failLogin, setFailLogin] = useState('');

    const {register, handleSubmit, errors} = useForm();

    const onSubmit = (data) => {
        dispatch(login(data));
    }
    useEffect(() => {
        if (isAuthenticated) {
            dispatch(closeModals());
        }
        if(authError) {
            setFailLogin('Логин или пароль неверный')
        }
    }, [isAuthenticated, authError]);

    return (
        <div className={`${a.forms_wrapper} ${l.form_height}`}>
            <CloseButton dispatch={dispatch}/>

            <form className={l.login_form} onSubmit={handleSubmit(onSubmit)}>

                <h5 className={`${l.title} ${l.login__title}`}>Вход</h5>
                <p className={`${l.error} ${l.top_margin}`}>{failLogin}</p>
                <div className={l.field}>
                    <input type="text"
                           id="user"
                           name="username"
                           ref={register({required: "Это поле обязательно для заполнения."})}/>
                    <label htmlFor="user">Имя пользователя</label>
                    {errors.username && <p className={l.error}>{errors.username.message}</p>}
                </div>
                <div className={`${l.field} ${l.password}`}>
                    <input
                        type="password"
                        id="password"
                        name="password"
                        ref={register({
                            required: "Это поле обязательно для заполнения.",
                            minLength: {value: 3, message: "Too short"}
                        })}/>
                    <label htmlFor="password">Пароль</label>
                    {errors.password && <p className={l.error}>{errors.password.message}</p>}
                    {/*{signUpErrors.password && signUpErrors.password.map(el => <p className={l.error}>{el}</p>)}*/}
                </div>

                <div className={l.field}>
                    <button className={l.btn}>Войти</button>
                </div>
            </form>
            <div className={l.account}>
                <span>Нет аккаунта?</span>
                <span className={t.toggle_form}
                      onClick={() => dispatch(toggleSignUpModal())}
                >
                 Зарегистрируйтесь
                </span>
            </div>
            {/*<ToggleFormButton setIsSignUp = { props.setIsSignUp} setIsLogin={props.setIsLogin} login={true}/>*/}
        </div>
    )
}

export default Login;