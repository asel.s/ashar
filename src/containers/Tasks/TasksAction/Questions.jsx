import React from "react";
import t from "../Tasks.module.css";
import ask from "../../../assets/images/ask.png";
import Button from "../../../components/Button/Button";


const Questions = () => {
    return (
        <div className={t.questions}>
            <div className={t.questions__photo}>
                <img className={t.questions__img} src={ask} alt=""/>
                <p className={t.questions__label}>Частые вопросы исполнителей</p>
            </div>
            <div className={t.questions__info}>
                <p className={t.questions__text}>Как я получу оплату за
                    выполненый заказ?
                </p>
            </div>
            <Button
                title="Открыть все вопросы"
                color="#FFFFFF"
            />
        </div>
    )
}

export default Questions;