import {taskAPI} from "../../  api/api";
import {
    CREATE_TASK_LOADING,
    CREATE_TASK_SUCCESS,
    DELETE_LOADING,
    DELETE_TASK,
    DELETE_TASK_SUCCESS, EDIT_TASK_LOADING, EDIT_TASK_SUCCESS,
    SET_ALL_TASKS
} from "./action-types";

import {returnErrors} from "./error-action";

export const addTaskName = name => ({
    type: 'ADD_TASK_NAME',
    payload: name
});


export const createTask = (data) => (dispatch, getState) => {

    dispatch(createTaskLoading());

    taskAPI.createTask(data, getState)
        .then(response => {
            if(response.status === 201) {
                dispatch({
                    type: CREATE_TASK_SUCCESS,
                    payload:  response.data
                });
            }
        }).catch(error => {
        // dispatch(returnErrors(error.response.data,
        //     error.response.status));
    })
}

export const getTasks = () => (dispatch) => {
    taskAPI.getTask()
        .then(response => {
            dispatch({
                type: SET_ALL_TASKS,
                payload: response.data
            })
        }).catch(error => {
        // dispatch(returnErrors(error.response.data,
        //     error.response.status));
    })
}

export const deleteTask = (taskId) => (dispatch, getState) => {
    dispatch(deleteTaskLoading());

    taskAPI.deleteTask(taskId, getState)
        .then(response => {
            dispatch({
                type: DELETE_TASK_SUCCESS,
            })
        }).catch(error => {
        // dispatch(returnErrors(error.response.data,
        //     error.response.status));
    })
}

export const editTask = (data) => (dispatch) => {
    dispatch(editTaskLoading());

    taskAPI.editTask(data)
        .then(response => {
            dispatch({
                type: EDIT_TASK_SUCCESS,
                payload: response.data
            })
        }).catch(error => {
        // dispatch(returnErrors(error.response.data,
        //     error.response.status));
    })
}

export const createTaskLoading = () => {
    return {
        type: CREATE_TASK_LOADING
    }
};

export const deleteTaskLoading = () => {
    return {
        type: DELETE_LOADING
    }
};
export const editTaskLoading = () => {
    return {
        type: EDIT_TASK_LOADING
    }
}