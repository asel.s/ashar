const initialState = {
    order: [
        {id: 1, user: 'Petya', work: 'haircat'}
    ]
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ORDERS_LOADED':
            return {
                order: action.payload
            }
        default:
                return state;
    }
}

export default reducer;

