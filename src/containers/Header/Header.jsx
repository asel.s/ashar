import React, { useState} from 'react';
import h from './Header.module.css';
import {NavLink} from "react-router-dom";
import Auth from "./Auth/Auth";
import logo from "../../assets/images/logo-1.png"
import Dropdown from "./Dropdown/Dropdown";
import {useSelector, useDispatch} from "react-redux";
import AuthButton from "./AuthButton/AuthButton";

const Header = (props) => {

    const dropdown = useSelector(state => state.auth.isDropdown);
    const user = useSelector(state => state.auth.user);
    const signUpModal = useSelector(state => state.auth.isSignUpModal);
    const loginModal = useSelector(state => state.auth.isLoginModal);
    const dispatch = useDispatch();

    return (
        <div className={h.header}>
            <nav className={h.nav}>
                <div className={h.logo}>
                    <NavLink to="/" className={h.link}> <img className={h.image} src={logo} alt="logo"/> </NavLink>
                </div>
                <ul className={h.list}>
                    <li className={h.item}>
                        <NavLink to="/createTask" className={h.link} activeStyle={{fontWeight: "bold", color: "#26A0F9" }}>Создать заказ</NavLink>
                    </li>
                    <li className={h.item}>
                        <NavLink to="/searchTask" className={h.link} activeStyle={{fontWeight: "bold", color: "#26A0F9" }}>Найти заказы</NavLink>
                    </li>
                    <li className={h.item}>
                        <NavLink to="/notFound" className={h.link} activeStyle={{fontWeight: "bold", color: "#26A0F9" }}>Контакты</NavLink>
                    </li>
                </ul>
                <div className={h.login}>
                    <AuthButton
                        dispatch={dispatch}
                        dropdown={dropdown}
                        user={user}
                    />

                    {
                        dropdown && <Dropdown
                        signUpModal={signUpModal}
                        loginModal={loginModal}
                        dropdown={dropdown}
                        dispatch={dispatch}
                    />
                    }
                </div>
                    { (loginModal || signUpModal) && <Auth signUpModal={signUpModal}/> }
            </nav>
        </div>
    )
}

export default Header;