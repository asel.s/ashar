import {GET_ERRORS, CLEAR_ERRORS} from "../actions/action-types";

// RETURN ERRORS

export const returnErrors = (msg, status) => {
    return {
        type: GET_ERRORS,
        payload: { msg, status }
    }
}

//CLEAR ERRORS

export const clearError = () => {
    return {
        type: CLEAR_ERRORS
    }
}