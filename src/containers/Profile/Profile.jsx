import React from "react";
import p from './profile.module.css'
import {useForm} from "react-hook-form";


const Profile = () => {

    const {register, handleSubmit, errors} = useForm();

    return (
        <div className={p.profile}>
            <h2 className={p.title}>Настройки профиля</h2>
            <form className={p.profile_settings}>
                <div className={p.form_group}>
                    <label htmlFor="input-name">Имя</label>
                    <input
                        id="input-name"
                        type="text"
                        name="name"
                        ref={register({required: "Required"})}/>
                </div>
                <div className={p.form_group}>
                    <label htmlFor="input-name">Фамилия</label>
                    <input
                        id="input-name"
                        type="text"
                        name="last_name"
                        ref={register({required: "Required"})}/>
                </div>
                <div className={p.form_group}>
                    <label htmlFor="input-phone">Телефон</label>
                    <input
                        id="input-phone"
                        type="text"
                        name="phone"
                        ref={register({required: "Required"})}
                    />
                </div>
                <div className={p.form_group}>
                    <label htmlFor="input-type">Тип пользователя</label>
                    <input
                        id="input-type"
                        type="text"
                        name="type_user"
                        ref={register({required: "Required"})}
                    />
                </div>
                <div className={p.form_group}>
                    <label htmlFor="input-rating">Рейтинг</label>
                    <input
                        id="input-rating"
                        type="text"
                        name="rating"
                        ref={register({required: "Required"})}
                    />
                </div>
                <div className={p.form_group}>
                    <label htmlFor="input-isApproved">Одобренный</label>
                    <input
                        id="input-isApproved"
                        type="text"
                        name="is_approved"
                        ref={register({required: "Required"})}
                    />
                </div>
                <button className={p.button}>Сохранить</button>
            </form>
        </div>
    )
}

export default Profile;