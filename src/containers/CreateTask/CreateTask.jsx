import React, {useState} from "react";
import c from './CreateTask.module.css';
import ServiceInstruction from "./ServiceInstruction/ServiceInstruction";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {useForm} from "react-hook-form";
import {useDispatch, useSelector} from "react-redux";
import {createTask, getTasks, setTask} from "../../redux/actions/tasks-action";
import {Redirect} from "react-router-dom";
import moment from "moment";


const CreateTask = () => {

    const defaultValues = {
        category: '',
        name: '',
        summary: '',
        phone_number: '',
        date: '',
        time: '',
        estimated_pay: '',
    };

    const {register, handleSubmit, errors} = useForm({defaultValues});
    const dispatch = useDispatch();
    const [startDate, setStartDate] = useState(new Date());
    const [time, setTime] = useState();
    const categories = useSelector(state => state.categories.categories);
    const taskId = useSelector(state => state.task.task.id);


    // JOIN DATE AND TIME

    let dateTime, date;

    date = moment(startDate).format('YYYY-MM-DD');

    if (!time) {
        let timeDefault = new Date().toString().slice(15, 24);
        dateTime = date.concat(timeDefault);
    } else {
        dateTime = date.concat(time.toLocaleString().slice(10, 19));
    }

    const onSubmit = (data) => {
        data = {...data, date_time: dateTime};
        dispatch(createTask(data));
    }

    // if(taskId) {
    //    return  <Redirect to={`/history/${taskId}`} />
    // }

    return (
        <div className={c.createTask}>
            <form className={c.form} onSubmit={handleSubmit(onSubmit)}>
                <h2 className={c.title}>Cоздайте заказ</h2>

                <div className={c.form__group}>
                    <label>Выбраная категория: <br/> </label>
                    <select
                        name="category"
                        className={c.category}
                        ref={register({required: "Required"})}
                    >
                        {categories.map((el, index) => <option key={index} value={el.id}>{el.name}</option>)}
                    </select>
                </div>

                <div className={c.form__group}>
                    <label>Заголовок</label>
                    <input
                        type="text"
                        name="name"
                        className={c.task_name}
                        ref={register({required: "Required"})}
                    />
                </div>

                <div className={c.form__group}>
                    <label>Опишите задание, детали помогут выполнить заказ лучше</label>
                    <textarea
                        name="summary"
                        className={c.summary}
                        ref={register({required: "Required"})}
                    />
                </div>

                <div className={`${c.form__group}`}>
                    <label>Телефон</label><br/>
                    <input
                        type="text"
                        name="phone_number"
                        className={c.phone_number}
                        ref={register({required: "Required"})}
                    />
                </div>

                <div className={`${c.form__group} ${c.inline}`}>
                    <div className={c.date_wrap}>
                        <label>Дата</label>
                        <DatePicker
                            selected={startDate}
                            onChange={date => setStartDate(date)}
                            minDate={new Date()}
                            showDisabledMonthNavigation
                            placeholder="Сегодня"
                            dateFormat='dd/MM'
                            ref={register({required: "Required"})}
                        />
                    </div>

                    <div className={c.time_wrap}>
                        <label>Время</label>
                        <DatePicker
                            selected={time}
                            onChange={e => setTime(e)}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            timeCaption="Time"
                            dateFormat="HH:mm"
                            name="time"
                        />
                    </div>
                </div>

                <div className={c.form__group}>
                    <label>Укажите цену</label>
                    <input
                        type="text"
                        name="estimated_pay"
                        className={c.price}
                        placeholder="500"
                        ref={register({required: "Required"})}
                    />
                </div>

                <button type="submit" className={c.button}>Сделать заказ</button>
            </form>
            <div className={c.aboutService}>
                <ServiceInstruction/>
            </div>
        </div>
    )
}

export default CreateTask;