import React, {useEffect} from "react";
import t from "./ToggleFormButton.module.css"


const ToggleFormButton = (props) => {

    const {login, signup} = props;

    return (
        <div className={t.btn_block}>
            <span className={t.ask}>Есть аккаунт?</span>
            <span className={t.btn__item}
                  onClick={ () => props.setIsSignUp(true) }
            >
                 Зарегистрируйтесь
                {props.signup ? 'Войдите' : 'Зарегистрируйтесь'}

            </span>
        </div>
    )
}

export default ToggleFormButton;