import React from "react";
import a from "./AuthButton.module.css";
import {toggleDropdownMenu, toggleLoginModal} from "../../../redux/actions/auth-action";


const AuthButton = ({dropdown, user, dispatch}) => {

    const handleClick = () => {
        dispatch(toggleLoginModal());
        document.body.style.overflow = 'hidden';
    }

    return (
        <div>
            {!user
                ? <button
                    className={dropdown ? `${a.btn_active} + ${a.btn_login}` : `${a.btn} + ${a.btn_login}`}>
                    <span onClick={handleClick}>Войти</span>
                    <span className={a.btn_label} onClick={() => dispatch(toggleDropdownMenu(!dropdown))}>
                             <i className="fas fa-chevron-down"/>
                    </span>
                </button>
                : <button
                    className={dropdown ? `${a.btn_active} + ${a.btn_logout}` : `${a.btn} + ${a.btn_logout}`}>
                    <span>
                        <i className="far fa-user"/>
                    </span>

                    <span className={a.btn_label} onClick={() => dispatch(toggleDropdownMenu(!dropdown))}>
                          <i className="fas fa-chevron-down"/>
                    </span>
                </button>

            }
        </div>
    )
}

export default AuthButton;